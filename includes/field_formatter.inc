<?php
/**
 * @file
 * Field formatter.
 */

/**
 * Implements hook_field_formatter_info().
 */
function anu_content_styles_field_formatter_info() {
  return array(
    'anu_datetime_format' => array(
      'label' => t('ANU: date format'),
      'description' => t('Formats a date/time value according to the ANU Editorial Style Guide.'),
      'field types' => array('date', 'datestamp', 'datetime'),
      'settings' => array(
        'granularity' => 'default',
        'show_parts' => array(),
        'weekday_abbr' => 'no',
        'month_abbr' => 'no',
        'time_position' => 'before',
        'time_separator' => '',
        'fromto' => 'both',
      ),
    ),
    'anu_call_to_action' => array(
      'label' => t('ANU: call to action'),
      'description' => t('Formats a call-to-action link.'),
      'field types' => array('link_field', 'email', 'file'),
      'settings' => array(
        'text' => '',
        'link_text' => FALSE,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function anu_content_styles_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  switch ($display['type']) {
    case 'anu_datetime_format':
      return anu_content_styles_field_formatter_datetime_view($entity_type, $entity, $field, $instance, $langcode, $items, $display);
    case 'anu_call_to_action':
      return anu_content_styles_field_formatter_call_to_action_view($entity_type, $entity, $field, $instance, $langcode, $items, $display);
  }
  return array();
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function anu_content_styles_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  switch ($instance['display'][$view_mode]['type']) {
    case 'anu_datetime_format':
      return anu_content_styles_field_formatter_datetime_settings_form($field, $instance, $view_mode, $form, $form_state);
    case 'anu_call_to_action':
      return anu_content_styles_field_formatter_call_to_action_settings_form($field, $instance, $view_mode, $form, $form_state);
  }
  return array();
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function anu_content_styles_field_formatter_settings_summary($field, $instance, $view_mode) {
  switch ($instance['display'][$view_mode]['type']) {
    case 'anu_datetime_format':
      return anu_content_styles_field_formatter_datetime_settings_summary($field, $instance, $view_mode);
    case 'anu_call_to_action':
      return anu_content_styles_field_formatter_call_to_action_settings_summary($field, $instance, $view_mode);
  }
  return '';
}

/**
 * Implements hook_field_formatter_settings_form() for date format.
 */
function anu_content_styles_field_formatter_datetime_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $settings = $instance['display'][$view_mode]['settings'];

  $element['granularity'] = array(
    '#type' => 'radios',
    '#title' => t('Show date attributes using'),
    '#options' => array(
      'default' => t('Default field settings'),
      'custom' => t('Custom configuration'),
    ),
    '#default_value' => $settings['granularity'],
    '#id' => drupal_html_id('edit-anu-date-granularity'),
  );

  $dependencies = anu_content_styles_date_display_dependencies();
  $possible_parts = $parts = anu_content_styles_date_get_parts_from_granularity(array_filter($field['settings']['granularity']));
  foreach ($parts as $part) {
    foreach ($dependencies as $dependent => $dependency) {
      if (in_array($part, $dependency)) {
        $possible_parts[] = $dependent;
      }
    }
  }
  $sequence = anu_content_styles_date_display_sequence();
  $parts_options = array_intersect_key($sequence, array_flip($possible_parts));
  $granularity_selector = '#' . $element['granularity']['#id'];
  $element['show_parts'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Show these date attributes'),
    '#options' => $parts_options,
    '#default_value' => !empty($settings['show_parts']) ? $settings['show_parts'] : $parts,
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        "$granularity_selector :radio" => array('value' => 'custom'),
      ),
    ),
    '#process' => array('form_process_checkboxes', 'anu_content_styles_date_customize_parts_element'),
    '#id' => drupal_html_id('edit-anu-date-show-parts'),
  );

  $element['weekday_abbr'] = array(
    '#title' => t('Abbreviate weekday name (e.g. Tue, Fri)'),
    '#type' => 'radios',
    '#options' => array(
      'no' => t('No'),
      'yes' => t('Yes'),
      'range' => t('Only in a date range'),
    ),
    '#default_value' => $settings['weekday_abbr'],
  );

  $element['month_abbr'] = array(
    '#title' => t('Abbreviate month name (e.g. Mar, Dec)'),
    '#type' => 'radios',
    '#options' => array(
      'no' => t('No'),
      'yes' => t('Yes'),
      'range' => t('Only in a date range'),
    ),
    '#default_value' => $settings['month_abbr'],
  );

  $parts_selector = '#' . $element['show_parts']['#id'];
  $element['time_position'] = array(
    '#title' => t('Time display position'),
    '#type' => 'radios',
    '#options' => array(
      'before' => t('Before'),
      'after' => t('After'),
    ),
    '#default_value' => $settings['time_position'],
    '#states' => array(
      'visible' => array(
        "$parts_selector input[value=time]" => array('checked' => TRUE),
      ),
    ),
  );
  $element['time_separator'] = array(
    '#title' => t('Separator between date and time (in addition to space)'),
    '#type' => 'textfield',
    '#size' => 5,
    '#default_value' => $settings['time_separator'],
    '#states' => array(
      'visible' => array(
        "$parts_selector input[value=time]" => array('checked' => TRUE),
      ),
    ),
  );

  if (!empty($field['settings']['todate'])) {
    $element['fromto'] = array(
      '#title' => t('Display'),
      '#type' => 'select',
      '#options' => array(
        'both' => t('Range from start to end date'),
        'from' => t('Start date only'),
        'to' => t('End date only'),
      ),
      '#access' => $field['settings']['todate'],
      '#default_value' => $settings['fromto'],
      '#weight' => 1,
    );
  }

  return $element;
}

/**
 * Customize date parts element.
 */
function anu_content_styles_date_customize_parts_element($element) {
  $element['daymonth']['#attributes']['class'][] = 'anu-date-parts-daymonth';
  $element['year']['#attributes']['class'][] = 'anu-date-parts-year';
  $element['weekday']['#states'] = array(
    'visible' => array(
      '.anu-date-parts-daymonth' => array('checked' => TRUE),
    ),
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary() for date format.
 */
function anu_content_styles_field_formatter_datetime_settings_summary($field, $instance, $view_mode) {
  $summary[] = t('Display dates according to the ANU Editorial Style Guide');
  $display_settings = $instance['display'][$view_mode]['settings'];

  // Summarize parts configured for display.
  $parts = anu_content_styles_date_field_show_parts(date_granularity($field), $display_settings);
  $labels = array_intersect_key(anu_content_styles_date_display_sequence($display_settings['time_position'] == 'after'), $parts);
  if (isset($labels['time']) && strlen($display_settings['time_separator'])) {
    $labels['time'] = t('!time (separated by "@separator")', array('!time' => $labels['time'], '@separator' => $display_settings['time_separator']));
  }

  $parts_list = t('%part', array('%part' => reset($labels)));
  while ($label = next($labels)) {
    $parts_list = t('!list, %part', array('!list' => $parts_list, '%part' => $label));
  }

  $summary[] = t('Show: !parts.', array('!parts' => $parts_list));

  if (isset($labels['weekday'])) {
    if ($display_settings['weekday_abbr'] == 'no') {
      $summary[] = t('Show full weekday.');
    }
    elseif ($display_settings['weekday_abbr'] == 'yes') {
      $summary[] = t('Show abbreviated weekday.');
    }
    else {
      $summary[] = t('Show abbreviated weekday in a date range.');
    }
  }

  if (isset($labels['daymonth'])) {
    if ($display_settings['month_abbr'] == 'no') {
      $summary[] = t('Show full month name.');
    }
    elseif ($display_settings['month_abbr'] == 'yes') {
      $summary[] = t('Show abbreviated month name.');
    }
    else {
      $summary[] = t('Show abbreviated month name in a date range.');
    }
  }

  if ($display_settings['fromto'] == 'from') {
    $fromto = t('start dates only');
  }
  elseif ($display_settings['fromto'] == 'to') {
    $fromto = t('end dates only');
  }
  else {
    $fromto = t('date ranges from start to end');
  }
  $summary[] = t('Display @fromto.', array('@fromto' => $fromto));

  return implode('<br />', $summary);
}

/**
 * Determines date parts to show from settings.
 */
function anu_content_styles_date_field_show_parts(array $granularity, array $display_settings) {
  $display_settings += array('granularity' => 'default');

  // First determine what parts are actually available.
  $parts = anu_content_styles_date_get_parts_from_granularity($granularity);
  if ($display_settings['granularity'] == 'custom' && !empty($display_settings['show_parts'])) {
    $parts = array_intersect($parts, array_filter($display_settings['show_parts']));
  }
  else {
    // Hide weekday by default.
    if (FALSE !== $index = array_search('weekday', $parts)) {
      unset($parts[$index]);
      $parts = array_values($parts);
    }
  }

  // Trace dependencies.
  $dependencies = anu_content_styles_date_display_dependencies();
  foreach (array_intersect_key($dependencies, array_flip($parts)) as $dependency) {
    foreach ($dependency as $part) {
      if (in_array($part, $parts)) {
        $parts = array_merge($parts, $dependency);
      }
    }
  }

  // Order date parts in sequence.
  $sequence = array_keys(anu_content_styles_date_display_sequence($display_settings['time_position'] == 'after'));
  $parts = array_intersect($sequence, $parts);
  return drupal_map_assoc($parts);
}

/**
 * Returns a map of individual date display units to date granularity levels.
 *
 * This is typically used to check whether a unit ranges across date values.
 */
function anu_content_styles_date_units_granularity() {
  return array(
    'time' => array('hour', 'minute', 'second'),
    'weekday' => array('day', 'month', 'year'),
    'day' => array('day'),
    'month' => array('month'),
    'year' => array('year'),
  );
}

/**
 * Returns a list of groups of date display units.
 *
 * This is typically used to determine the groups of date units that range.
 */
function anu_content_styles_date_parts() {
  return array(
    'time' => array('time'),
    'weekday' => array('weekday'),
    'daymonth' => array('day', 'month'),
    'year' => array('year'),
  );
}

/**
 * Returns a map of date display parts groups to date granularity levels.
 */
function anu_content_styles_date_parts_granularity() {
  $granularity = &drupal_static(__FUNCTION__);
  if (!isset($cache)) {
    $granularity = array();
    $units_granularity = anu_content_styles_date_units_granularity();
    foreach (anu_content_styles_date_parts() as $part => $units) {
      $granularity[$part] = array();
      foreach ($units as $unit) {
        $granularity[$part] = array_merge($granularity[$part], $units_granularity[$unit]);
      }
      $granularity[$part] = array_unique($granularity[$part]);
    }
  }
  return $granularity;
}

/**
 * Returns a map of date display parts to date granularity levels.
 */
function anu_content_styles_date_get_parts_from_granularity(array $granularity) {
  $map = anu_content_styles_date_parts_granularity();
  $parts = array();
  foreach ($map as $part => $match_granularity) {
    if (array_intersect($match_granularity, $granularity)) {
      $parts[] = $part;
    }
  }
  return $parts;
}

/**
 * Returns a map of date display parts to date granularity levels.
 */
function anu_content_styles_date_get_granularity_from_parts(array $parts) {
  $map = anu_content_styles_date_parts_granularity();
  $granularity = array();
  foreach ($parts as $part) {
    if (!empty($map[$part])) {
      $granularity = array_merge($granularity, $map[$part]);
    }
  }
  return array_unique($granularity);
}

/**
 * Returns a list of dependencies between date display parts.
 */
function anu_content_styles_date_display_dependencies() {
  return array(
    'weekday' => array('daymonth'),
  );
}

/**
 * Returns a list of sequential display parts options.
 */
function anu_content_styles_date_display_sequence($time_after_date = FALSE) {
  $time = array(
    'time' => t('Time'),
  );
  $date = array(
    'weekday' => t('Weekday'),
    'daymonth' => t('Day and month'),
    'year' => t('Year'),
  );
  return $time_after_date ? ($date + $time) : ($time + $date);
}

/**
 * Implements hook_field_formatter_view() for date format.
 */
function anu_content_styles_field_formatter_datetime_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'] + array(
      'granularity' => 'default',
      'show_parts' => array(),
      'weekday_abbr' => 'no',
      'month_abbr' => 'no',
      'time_position' => 'before',
      'time_separator' => '',
      'fromto' => 'both',
    );

  $parts = anu_content_styles_date_field_show_parts(array_filter($field['settings']['granularity']), $settings);
  foreach ($items as $delta => $item) {
    if (!empty($field['settings']['todate']) && $settings['fromto'] == 'both') {
      $variables = array(
        'date_from' => anu_content_styles_get_date_object($item, 'value'),
        'date_to' => anu_content_styles_get_date_object($item, 'value2'),
        'parts' => $parts,
        'weekday_abbr' => $settings['weekday_abbr'],
        'month_abbr' => $settings['month_abbr'],
        'time_position' => $settings['time_position'],
        'time_separator' => $settings['time_separator'],
      );
      $output = theme('anu_date_display_range', $variables);
    }
    else {
      $item_key = (empty($field['settings']['todate']) || $settings['fromto'] == 'from') ? 'value' : 'value2';
      $variables = array(
        'date' => anu_content_styles_get_date_object($item, $item_key),
        'parts' => $parts,
        'weekday_abbr' => $settings['weekday_abbr'],
        'month_abbr' => $settings['month_abbr'],
        'time_position' => $settings['time_position'],
        'time_separator' => $settings['time_separator'],
      );
      $output = theme('anu_date_display', $variables);
    }

    $element[$delta] = array(
      '#markup' => $output,
    );
  }

  return $element;
}

/**
 * Creates a date object from a field item.
 */
function anu_content_styles_get_date_object($item, $key = 'value') {
  if (module_exists('date_api')) {
    if (isset($item['db'][$key])) {
      $date = $item['db'][$key];
    }
    else {
      if (!empty($item['timezone']) && empty($item['timezone_db'])) {
        $item['timezone_db'] = 'UTC';
      }
      $format = date_type_format($item['date_type']);
      $date = new DateObject($item[$key], $item['timezone_db'], $format);
    }
    $date->setTimezone(new DateTimeZone($item['timezone']));
    return $date;
  }
}

/**
 * Displays a date.
 */
function theme_anu_date_display($variables) {
  return theme('anu_date_display_range', array(
    'date_from' => $variables['date'],
    'date_to' => $variables['date'],
    'parts' => $variables['parts'],
    'weekday_abbr' => $variables['weekday_abbr'],
    'month_abbr' => $variables['month_abbr'],
    'time_position' => $variables['time_position'],
    'time_separator' => $variables['time_separator'],
  ));
}

/**
 * Displays a date range.
 */
function theme_anu_date_display_range($variables) {
  if (!module_exists('date_api')) {
    return '';
  }

  /** @var $from DateObject */
  $from = $variables['date_from']->toArray();
  /** @var $to DateObject */
  $to = $variables['date_to']->toArray();

  // Derive display units.
  $range_units = array();
  $units = array();
  $parts = array_intersect_key(anu_content_styles_date_parts(), $variables['parts']);
  $map = anu_content_styles_date_units_granularity();
  $time_after_date = $variables['time_position'] == 'after';
  $empty_time = TRUE;
  $range_entire_date = FALSE;
  foreach ($parts as $part => $part_units) {
    // Skip weekday if day/month is not shown.
    if ($part == 'weekday' && !isset($parts['daymonth'])) {
      continue;
    }
    // Process units by granularity.
    $range_part = FALSE;
    foreach ($part_units as $unit) {
      if (isset($map[$unit])) {
        foreach ($map[$unit] as $granularity) {
          // Determine whether time is empty.
          if ($part == 'time') {
            $empty_time = $empty_time && ($from[$granularity] == 0) && ($to[$granularity] == 0);
          }
          // Range the whole part if any unit ranges.
          if ($from[$granularity] != $to[$granularity]) {
            $range_part = TRUE;
          }
        }
      }
    }
    // Skip empty time.
    if ($part == 'time' && $empty_time) {
      continue;
    }
    // Range entire date if day is ranged and time is displayed after date.
    if (!$empty_time && $part != 'time' && $time_after_date && $range_part) {
      $range_entire_date = TRUE;
    }
    // Add units.
    $units = array_merge($units, $part_units);
    // Add ranging units.
    if ($range_entire_date || $range_part) {
      $range_units = array_merge($range_units, $part_units);
    }
  }

  // Range units to the smallest available part.
  $units_index = array_flip($units);
  $last_range_unit = -1;
  if (!empty($range_units)) {
    $last_range_unit = $units_index[end($range_units)];
    $range_units = array_values(array_intersect_key($units, array_flip(range(0, $last_range_unit))));
  }

  // Determine unranged units.
  if (empty($range_units)) {
    $single_units = $units;
  }
  elseif ($last_range_unit + 1 < count($units)) {
    $single_units = array_values(array_intersect_key($units, array_flip(range($last_range_unit + 1, count($units) - 1))));
  }
  else {
    $single_units = array();
  }

  // Range only day for same month if not ranging less than day.
  if ($range_units == array('day', 'month') && $from['month'] == $to['month']) {
    $range_units = array('day');
    array_unshift($single_units, 'month');
  }

  // Modify time display.
  $invert_groups = FALSE;
  $add_separator = strlen($variables['time_separator']) && $units != array('time');
  if (!$empty_time) {
    // Case 1: Date is unranged.
    if (empty($range_units)) {
      // The first unit must be time.
      array_shift($single_units);
      if ($time_after_date) {
        if ($add_separator) {
          $single_units[] = 'separator';
        }
        $single_units[] = 'time';
      }
      else {
        array_unshift($single_units, 'separator');
        array_unshift($single_units, 'time');
      }
    }
    // Case 2: Only time is ranged.
    elseif ($range_units == array('time')) {
      if ($time_after_date) {
        $invert_groups = TRUE;
        if ($add_separator) {
          $single_units[] = 'separator';
        }
      }
      elseif ($add_separator) {
        array_unshift($single_units, 'separator');
      }
    }
    // Case 3: More than time is ranged.
    else {
      // The first unit must be time.
      array_shift($range_units);
      if ($time_after_date) {
        if ($add_separator) {
          $range_units[] = 'separator';
        }
        $range_units[] = 'time';
      }
      else {
        array_unshift($range_units, 'separator');
        array_unshift($range_units, 'time');
      }
    }
  }

  // Build parts to display.
  $display_units = array();
  if ($range_units) {
    $display_units[] = theme('anu_date_display_units_range', array(
      'units' => $range_units,
      'date_from' => $variables['date_from'],
      'date_to' => $variables['date_to'],
      'weekday_abbr' => in_array($variables['weekday_abbr'], array('yes', 'range'), TRUE),
      'month_abbr' => in_array($variables['month_abbr'], array('yes', 'range'), TRUE),
      'separator' => $variables['time_separator'],
    ));
  }
  if ($single_units) {
    $single_output = theme('anu_date_display_units', array(
      'units' => $single_units,
      'date' => $variables['date_from'],
      'weekday_abbr' => $variables['weekday_abbr'] == 'yes',
      'month_abbr' => $variables['month_abbr'] == 'yes',
      'separator' => $variables['time_separator'],
    ));
    if ($invert_groups) {
      array_unshift($display_units, $single_output);
    }
    else {
      $display_units[] = $single_output;
    }
  }

  $output = theme('anu_date_display_group', array('group' => $display_units));
  return $output;
}

/**
 * Displays a group of rendered date units.
 */
function theme_anu_date_display_group($variables) {
  return implode(' ', $variables['group']);
}

/**
 * Displays a set of date units.
 */
function theme_anu_date_display_units($variables) {
  $display_units = array();
  foreach ($variables['units'] as $unit) {
    if ($unit == 'separator') {
      // Attach separator to the previous item.
      if (!empty($display_units)) {
        $display_units[count($display_units) - 1] .= $variables['separator'];
      }
    }
    else {
      $display_units[] = theme('anu_date_display_unit', array(
        'unit' => $unit,
        'date' => $variables['date'],
        'weekday_abbr' => $variables['weekday_abbr'],
        'month_abbr' => $variables['month_abbr'],
      ));
    }
  }
  return theme('anu_date_display_group', array('group' => $display_units));
}

/**
 * Displays a range of date units.
 */
function theme_anu_date_display_units_range(array $variables) {
  $units = $variables['units'];

  // Range single unit using en dash with no spaces.
  if (count($units) == 1) {
    $output = theme('anu_date_display_unit_range', array(
      'unit' => reset($units),
      'date_from' => $variables['date_from'],
      'date_to' => $variables['date_to'],
      'weekday_abbr' => $variables['weekday_abbr'],
      'month_abbr' => $variables['month_abbr'],
    ));
  }
  // Range multiple units using en dash with spaces.
  else {
    $output_from = theme('anu_date_display_units', array(
      'units' => $units,
      'date' => $variables['date_from'],
      'weekday_abbr' => $variables['weekday_abbr'],
      'month_abbr' => $variables['month_abbr'],
      'separator' => $variables['separator'],
    ));
    $output_to = theme('anu_date_display_units', array(
      'units' => $units,
      'date' => $variables['date_to'],
      'weekday_abbr' => $variables['weekday_abbr'],
      'month_abbr' => $variables['month_abbr'],
      'separator' => $variables['separator'],
    ));
    $output = t('@from &ndash; @to', array('@from' => $output_from, '@to' => $output_to));
  }

  return $output;
}

/**
 * Displays a single date unit.
 */
function theme_anu_date_display_unit($variables) {
  /** @var $date DateObject */
  $date = $variables['date'];
  switch ($variables['unit']) {
    case 'time':
      return anu_date_format_time($date);

    case 'weekday':
      if (module_exists('date_api')) {
        $weekdays = $variables['weekday_abbr'] ? date_week_days_abbr() : date_week_days();
        return $weekdays[(int) $date->format('w')];
      }
      else {
        return $date->format('l');
      }

    case 'day':
      return $date->format('j');

    case 'month':
      if (module_exists('date_api')) {
        $months = $variables['month_abbr'] ? date_month_names_abbr() : date_month_names();
        return $months[(int) $date->format('n')];
      }
      else {
        return $date->format($variables['month_abbr'] ? 'M' : 'F');
      }

    case 'year':
      return $date->format('Y');
  }
}

/**
 * Displays a date unit range.
 */
function theme_anu_date_display_unit_range($variables) {
  /** @var $date_from DateObject */
  $date_from = $variables['date_from'];
  /** @var $date_to DateObject */
  $date_to = $variables['date_to'];

  $text = '@from&ndash;@to';
  $text_args = array();
  switch ($variables['unit']) {
    case 'time':
      $from_ampm = $date_from->format('a');
      $to_ampm = $date_to->format('a');
      if ($from_ampm == $to_ampm) {
        $text = '@from&ndash;@to@ampm';
        $text_args['@from'] = anu_date_format_time($date_from, FALSE);
        $text_args['@to'] = anu_date_format_time($date_to, FALSE);
        $text_args['@ampm'] = $from_ampm;
      }
      else {
        $text_args['@from'] = anu_date_format_time($date_from);
        $text_args['@to'] = anu_date_format_time($date_to);
      }
      break;

    default:
      $text_args['@from'] = theme('anu_date_display_unit', array('unit' => $variables['unit'], 'date' => $date_from));
      $text_args['@to'] = theme('anu_date_display_unit', array('unit' => $variables['unit'], 'date' => $date_to));
      break;
  }

  return t($text, $text_args);
}

/**
 * Formats the time according to the ANU style.
 */
function anu_date_format_time($date, $include_ampm = TRUE) {
  $output = $date->format('g.i.s');
  while (substr($output, -3) == '.00') {
    $output = substr($output, 0, -3);
  }
  if ($include_ampm) {
    $output .= $date->format('a');
  }
  return $output;
}

/**
 * Implements hook_field_formatter_view() for call to action.
 */
function anu_content_styles_field_formatter_call_to_action_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $settings = $display['settings'] + array('text' => '', 'link_text' => FALSE);
  $text = check_plain($settings['text']);
  $text = token_replace($text, array($entity_type => $entity), array('clear' => TRUE, 'sanitize' => TRUE));

  $element = array();
  foreach ($items as $delta => $item) {
    $path = '';
    $title = $text;
    switch ($field['type']) {
      case 'link_field':
        $path = $item['url'];
        if (!empty($settings['link_text'])) {
          $title = $item['title'];
        }
        break;
      case 'email':
        $path = 'mailto:' . $item['email'];
        break;
      case 'file':
        $path = file_create_url($item['uri']);
        break;
    }
    $element[$delta] = array(
      '#theme' => 'link',
      '#text' => $title,
      '#path' => $path,
      '#options' => array(
        'attributes' => array('class' => array('btn-action')),
        'html' => TRUE,
      ),
    );
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_form() for call to action.
 */
function anu_content_styles_field_formatter_call_to_action_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $settings = $instance['display'][$view_mode]['settings'];

  // Ditch the form array since it may contain the file display form.
  $element['text'] = array(
    '#type' => 'textfield',
    '#title' => t('Link text'),
    '#default_value' => $settings['text'],
    '#description' => t('The action message to display on the button link. Tokens are available for the entity containing this field.'),
  );

  if ($field['type'] == 'link_field') {
    $text_html_id = drupal_html_id('edit-formatter-settings-text');
    $element['link_text'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use link text'),
      '#default_value' => $settings['link_text'],
      '#weight' => -5,
      '#attributes' => array('id' => $text_html_id),
    );
    $element['text']['#states']['enabled']["#$text_html_id"] = array('checked' => FALSE);
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary() for date format.
 */
function anu_content_styles_field_formatter_call_to_action_settings_summary($field, $instance, $view_mode) {
  if (!$field['type'] == 'link_field' || empty($instance['display'][$view_mode]['settings']['link_text'])) {
    return t('Call to action link: @text', array('@text' => $instance['display'][$view_mode]['settings']['text']));
  }
  else {
    return t('Call to action link');
  }
}
