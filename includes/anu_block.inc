<?php
/**
 * @file
 * ANU box block.
 *
 * Implements ANU box styles for blocks.
 */

/**
 * Implementation of hook_preprocess_block().
 * Introduces ANU style settings to the block template.
 */
function anu_content_styles_preprocess_block(&$vars) {
  // Fetch settings
  $block = $vars['block'];
  if (!empty($block->module) && !empty($block->delta)) {
    // Fetch settings
    $settings = _anu_content_styles_block_fetch_settings($block->module, $block->delta);

    if (!empty($settings) && $settings['use_anu_styles']) {
      // Set ANU styles information
      $vars['anu_content_styles']['show_box_header'] = $settings['box']['show_title'] == 'header';
      $styles = array(
        $settings['box']['style'],
        $settings['border']['style'],
        $settings['border']['color'],
        $settings['content']['bg'],
        $settings['content']['fg'],
      );
      $vars['anu_content_styles']['header_style'] = $settings['box']['header_style'];
      $vars['anu_content_styles']['styles'] = array_filter($styles);
    }
  }
}

/********************************
 * Form overrides.
 ********************************/

/**
 * Implementation of hook_form_alter().
 * Add configuration form to new block page.
 */
function anu_content_styles_form_block_add_block_form_alter(&$form, &$form_state) {
  _anu_content_styles_block_insert_form($form, $form_state);
}

/**
 * Implementation of hook_form_alter().
 * Add configuration form to block edit page.
 */
function anu_content_styles_form_block_admin_configure_alter(&$form, &$form_state) {
  _anu_content_styles_block_insert_form($form, $form_state);
}

/**
 * Internal use only.
 * Inserts the form into existing block form.
 */
function _anu_content_styles_block_insert_form(&$form, &$form_state) {
  // Put the main block editing fieldset at the top
  $form['block_settings']['#weight'] = -20;
  // Make new fieldset
  $form['anu_content_styles'] = array(
    '#type' => 'fieldset',
    '#title' => t('ANU styles'),
    '#tree' => TRUE,
    '#weight' => -1,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['anu_content_styles']['use_anu_styles'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display this block using ANU styles'),
    '#description' => t('If this option is deselected, none of the following settings will be saved.'),
    '#weight' => -10,
  );

  // Fetch settings
  $settings = _anu_content_styles_block_fetch_settings($form['module']['#value'], $form['delta']['#value']);
  
  // Override settings with form state
  if (!empty($form_state['values']['anu_content_styles'])) {
    $settings = $form_state['values']['anu_content_styles'];
  }

  if (!empty($settings)) {
    // Check the box above and expand the fieldset according to settings
    $form['anu_content_styles']['use_anu_styles']['#default_value'] = !empty($settings['use_anu_styles']);
    $form['anu_content_styles']['#collapsed'] = empty($settings['use_anu_styles']);
  }
  // Get form and push
  module_load_include('inc', 'anu_content_styles', 'includes/config_form');
  $form['anu_content_styles'] += anu_content_styles_config_form($settings);

  // Set validate and submit functions
  $form['#validate'][] = 'anu_content_styles_config_form_validate';
  $form['#submit'][] = 'anu_content_styles_form_block_submit';
}

/**
 * Form validate callback.
 */
function anu_content_styles_form_block_validate(&$form, &$form_state) {
  module_load_include('inc', 'anu_content_styles', 'includes/config_form');
  anu_content_styles_config_form_validate($form, $form_state);
}

/**
 * Form submit callback.
 * Saves settings for this block
 */
function anu_content_styles_form_block_submit(&$form, &$form_state) {
  $values = $form_state['values'];
  if (!empty($values['anu_content_styles'])) {
    // Get identifiers
    $module_id = "$values[module]:$values[delta]";
    // Fetch settings-related variables
    $settings = $values['anu_content_styles'];
    // Update values
    anu_content_styles_set('block', $module_id, $settings);
  }
}

/**
 * Internal use only.
 * Fetch settings.
 */
function _anu_content_styles_block_fetch_settings($module, $delta) {
  if ($element = anu_content_styles_get('block', "$module:$delta")) {
    // Fetch settings
    return $element->settings;
  } else {
    // Not found
    return array();
  }
}
