<?php
/**
 * @file
 * ANU styles pane/block/etc config form.
 *
 * Defines the config form for a content element using ANU styles.
 */

/**
 * Returns a form containing elements for configuring form.
 * Uses Form API.
 *
 * @param array $settings  Array of style settings organized in the same fashion as the form.
 * @return array  Configuration form.
 */
function anu_content_styles_config_form($settings) {
  module_load_include('inc', 'anu_content_styles', 'includes/style_config');
  $settings += array('box' => array());
  $settings['box'] += array(
    'show_title' => 'header',
    'header_style' => 'box-header',
    'style' => 'box-solid',
  );

  $form['box'] = array(
    '#type' => 'fieldset',
    '#title' => t('Box style'),
    '#tree' => TRUE,
  );
  $form['box']['show_title'] = array(
    '#type' => 'radios',
    '#title' => t('Title display'),
    '#prefix' => '<div class="anu-content-styles-show-title">',
    '#suffix' => '</div>',
    '#options' => array('content' => t('Show title in box content'), 'header' => t('Show title in box-header')),
    '#default_value' => (!empty($settings['box']['show_title']) ? $settings['box']['show_title'] : 'header'),
    '#description' => t('If <em>Show title in box-header</em> is selected, the title will be placed in a separate box above content, with background colour determined by the college/area of this website. Otherwise, the title will be placed as regular header text inside the box.'),
    '#required' => FALSE,
  );
  $form['box']['header_style'] = array(
    '#type' => 'select',
    '#title' => t('Box header style'),
    '#options' => anu_content_styles_box_header_styles(),
    '#default_value' => (!empty($settings['box']['header_style']) ? $settings['box']['header_style'] : ''),
    '#description' => t('Select the box header style to display. See the !styleguide for more information.', array('!styleguide' => l(t('ANU Web Style Guide - Box styles'), 'http://styles.anu.edu.au/guide/boxes.php', array('attributes' => array('target' => '_blank'))))),
    '#required' => FALSE,
    '#states' => array(
      'visible' => array(
        '.anu-content-styles-show-title :radio[name*=show_title]' => array('value' => 'header'),
      ),
    ),
  );
  $form['box']['header_level'] = array(
    '#type' => 'select',
    '#title' => t('Box heading level'),
    '#options' => array(
      '' => t('- None - '),
      'h2' => 'h2',
      'h3' => 'h3',
      'h4' => 'h4',
    ),
    '#default_value' => (!empty($settings['box']['header_level']) ? $settings['box']['header_level'] : ''),
    '#description' => t('Select the heading level to use for box header. See the !styleguide for more information.', array('!styleguide' => l(t('ANU Web Style Guide - Box styles'), 'http://styles.anu.edu.au/guide/boxes.php', array('attributes' => array('target' => '_blank'))))),
    '#required' => FALSE,
    '#states' => array(
      'visible' => array(
        '.anu-content-styles-show-title :radio[name*=show_title]' => array('value' => 'header'),
      ),
    ),
  );
  $form['box']['style'] = array(
    '#type' => 'select',
    '#title' => t('Box display style'),
    '#options' => array(NULL => '<None>') + anu_content_styles_box_styles(),
    '#default_value' => (!empty($settings['box']['style']) ? $settings['box']['style'] : ''),
    '#description' => t('Select the type of base box style to display. See the !styleguide for more information.', array('!styleguide' => l(t('ANU Web Style Guide - Box styles'), 'http://styles.anu.edu.au/guide/boxes.php', array('attributes' => array('target' => '_blank'))))),
    '#required' => FALSE,
  );

  $form['content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Box content style'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
  );
  $form['content']['bg'] = array(
    '#type' => 'select',
    '#title' => t('Background'),
    '#options' => array(
      NULL => '<Use display style background>',
      'Solid backgrounds' => anu_content_styles_bg_solid(),
      'Gradient backgrounds' => anu_content_styles_bg_gradient(),
    ),
    '#default_value' => (!empty($settings['content']['bg']) ? $settings['content']['bg'] : NULL),
    '#description' => t('Select the box background to override the box style above with. See the !styleguide for more information.', array('!styleguide' => l(t('ANU Web Style Guide - Backgrounds'), 'http://styles.anu.edu.au/guide/backgrounds.php', array('attributes' => array('target' => '_blank'))))),
    '#required' => FALSE,
  );
  $form['content']['fg'] = array(
    '#type' => 'select',
    '#title' => t('Text colour'),
    '#options' => array(NULL => '<Use display style text colour>') + anu_content_styles_text_colors(),
    '#default_value' => (!empty($settings['content']['fg']) ? $settings['content']['fg'] : NULL),
    '#description' => t('Select the text colour to override the box style above with. See the !styleguide for more information.', array('!styleguide' => l(t('ANU Web Style Guide - Colours'), 'http://styles.anu.edu.au/guide/colours.php', array('attributes' => array('target' => '_blank'))))),
    '#required' => FALSE,
  );
  $form['content']['#collapsed'] = empty($settings['content']['bg']) && empty($settings['content']['fg']);

  $form['border'] = array(
    '#type' => 'fieldset',
    '#title' => t('Box border'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
  );
  $form['border']['style'] = array(
    '#type' => 'select',
    '#title' => t('Border style'),
    '#options' => array(NULL => '<Use display style border>') + anu_content_styles_border_styles(),
    '#default_value' => (!empty($settings['border']['style']) ? $settings['border']['style'] : NULL),
    '#description' => t('Select the type of border to override the box style above with. See the !styleguide for more information.', array('!styleguide' => l(t('ANU Web Style Guide - Border styles'), 'http://styles.anu.edu.au/guide/borders.php', array('attributes' => array('target' => '_blank'))))),
    '#required' => FALSE,
  );
  $form['border']['color'] = array(
    '#type' => 'select',
    '#title' => t('Border colour'),
    '#options' => array(NULL => '<Use display style border colour>') + anu_content_styles_border_colors(),
    '#default_value' => (!empty($settings['border']['color']) ? $settings['border']['color'] : NULL),
    '#description' => t('Select the border colour to override the box style above with. See the !styleguide for more information.', array('!styleguide' => l(t('ANU Web Style Guide - Border styles'), 'http://styles.anu.edu.au/guide/borders.php', array('attributes' => array('target' => '_blank'))))),
    '#required' => FALSE,
  );
  $form['border']['#collapsed'] = empty($settings['border']['style']) && empty($settings['border']['color']);

  $form['box_css_classes'] = array(
    '#type' => 'textfield',
    '#title' => t('CSS classes'),
    '#default_value' => (!empty($settings['box_css_classes']) ? $settings['box_css_classes'] : NULL),
    '#description' => t('Extra CSS classes to be applied on the styled box, in addition to the styles specified above.'),
    '#required' => FALSE,
  );

  return $form;
}

/**
 * Variant of config form above, with title hidden.
 */
function anu_content_styles_titleless_config_form($settings) {
  $form = anu_content_styles_config_form($settings);
  $form['box']['show_title']['#access'] = FALSE;
  $form['box']['header_style']['#access'] = FALSE;
  $form['box']['header_level']['#access'] = FALSE;
  return $form;
}

/**
 * Validate callback.
 * Validates the submission of the config form.
 */
function anu_content_styles_config_form_validate(&$form, $form_state) {
  // Rule 1 : When used in isolation, a border style class must be used in combination with a border colour class.
  // http://styles.anu.edu.au/guide/borders.php
  if (empty($form_state['box']['style']) &&
      (empty($form_state['border']['style']) xor empty($form_state['border']['color']))
      ) {
    // Highlight one or the other
    /** @TODO Test if setting error to fieldset actually works and looks fine. */
    form_error($form['border'], t('When box style is not selected, both border style and colour must be specified.'));
  }
}
