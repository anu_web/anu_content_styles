<?php
/**
 * @file
 * ANU content styles configuration..
 *
 */

/**
 * Defines ANU box header styles. Box styles are the basis of a layout style.
 * @see ANU Web Style Guide - Box styles
 * @link http://styles.anu.edu.au/guide/boxes.php
 */
function anu_content_styles_box_header_styles() {
  static $headers = NULL;
  if ($headers === NULL) {
    $headers = array(
      'box-header' => t('Local colour header (.box-header)'),
      'box-header-uni' => t('Platinum header (.box-header-uni)'),
      'box-header-grey' => t('Grey header (.box-header-grey)'),
    );
  }
  return $headers;
}

/**
 * Defines ANU box styles. Box styles are the basis of a layout style.
 * @see ANU Web Style Guide - Box styles
 * @link http://styles.anu.edu.au/guide/boxes.php
 */
function anu_content_styles_box_styles() {
  static $boxes = NULL;
  if ($boxes === NULL) {
    $boxes = array(
      'box-solid' => t('Local colour box (.box-solid)'),
      'box-solid-uni' => t('Platinum box (.box-solid-uni)'),
      'box-solid-grey' => t('Grey box (.box-solid-grey)'),
      'box-bdr2' => t('Local colour top & bottom border (.box-bdr2)'),
      'box-bdr2-uni' => t('Platinum top & bottom border (.box-bdr2-uni)'),
      'box' => t('Box'),
      'box20' => t('Box with 20px padding (10px on smallest screen)'),
    );
  }
  return $boxes;
}

/**
 * Defines ANU backgrounds.
 * @see ANU Web Style Guide - Backgrounds
 * @link http://styles.anu.edu.au/guide/backgrounds.php
 */
function anu_content_styles_bg_solid() {
  static $backgrounds = NULL;
  if ($backgrounds === NULL) {
    $backgrounds = array();
    $colors = _anu_content_styles_colors();
    // Use background class prefix
    _anu_content_styles_colors_add_prefix_recursive($backgrounds, $colors, 'bg-');
  }
  return $backgrounds;
}

/**
 * Defines ANU gradients.
 * @see ANU Web Style Guide - Backgrounds
 * @link http://styles.anu.edu.au/guide/backgrounds.php
 */
function anu_content_styles_bg_gradient() {
  static $backgrounds = NULL;
  if ($backgrounds === NULL) {
    $backgrounds = array();
    $colors = _anu_content_styles_colors();
    // No gradients for black and white
    unset($colors['black']);
    unset($colors['white']);
    // Use gradient class prefix
    _anu_content_styles_colors_add_prefix_recursive($backgrounds, $colors, 'grd-');
  }
  return $backgrounds;
}

/**
 * Defines ANU text colors.
 * @see ANU Web Style Guide - Colours
 * @link http://styles.anu.edu.au/guide/colours.php
 */
function anu_content_styles_text_colors() {
  static $text_colors = NULL;
  if ($text_colors === NULL) {
    $text_colors = array();
    $colors = _anu_content_styles_colors();
    // Use text class prefix
    _anu_content_styles_colors_add_prefix_recursive($text_colors, $colors, 'text-');
  }
  return $text_colors;
}

/**
 * Defines ANU border styles.
 * @see ANU Web Style Guide - Border styles
 * @link http://styles.anu.edu.au/guide/borders.php
 */
function anu_content_styles_border_styles() {
  return array(
    'bdr-solid' => t('Solid'),
    'bdr-dotted' => t('Dotted'),
  );
}

/**
 * Defines ANU border colours.
 * @see ANU Web Style Guide - Border styles
 * @link http://styles.anu.edu.au/guide/borders.php
 */
function anu_content_styles_border_colors() {
  static $border_colors = NULL;
  if ($border_colors === NULL) {
    $border_colors = array();
    $colors = _anu_content_styles_colors();
    // Use border class prefix
    _anu_content_styles_colors_add_prefix_recursive($border_colors, $colors, 'bdr-');
  }
  return $border_colors;
}

/**
 * Internal use only.
 * Defines ANU colours. The array keys cannot be used themselves
 * @see ANU Web Style Guide - Colours
 * @link http://styles.anu.edu.au/guide/colours.php
 */
function _anu_content_styles_colors() {
  static $colors = NULL;
  if ($colors === NULL) {
    $colors = array(
      'ANU Platinum' => array(
        'uni' => t('ANU platinum 100%'),
        'uni50' => t('ANU platinum 50%'),
        'uni25' => t('ANU platinum 25%'),
        'uni10' => t('ANU platinum 10%'),
      ),
      'Charcoal grey' => array(
        'grey' => t('Charcoal grey 100%'),
        'grey50' => t('Charcoal grey 50%'),
        'grey25' => t('Charcoal grey 25%'),
        'grey10' => t('Charcoal grey 10%'),
      ),
      'black' => 'Black',
      'white' => 'White',
      'College colour' => array(
        'college' => t('College colour 100%'),
        'college50' => t('College colour 50%'),
        'college25' => t('College colour 25%'),
        'college10' => t('College colour 10%'),
      ),
    );
  }
  return $colors;
}

/**
 * Internal use only.
 * Recursively add prefixes to colour options.
 */
function _anu_content_styles_colors_add_prefix_recursive(&$options, $colors, $prefix) {
  $_path_queue = array();
  // Initial set
  $_array = $colors;
  foreach ($_array as $_key => $_value) {
    $_path_queue[] = array($_key);
  }
  // Recursively set
  while (NULL !== $_path = array_shift($_path_queue)) {
    // Navigate down to item
    $_item = $_array;
    $_depth = 0;
    foreach ($_path as $_path_segment) {
      $_item = $_item[$_path_segment];
      $_depth ++;
    }
    // If array, push; if string, set
    if (is_array($_item)) {
      foreach ($_item as $_key => $_value) {
        $_path_queue[] = array_merge($_path, array($_key));
      }
    } else {
      // ACTION: set color class prefix
      $option =& $options;
      foreach ($_path as $_i => $_path_segment) {
        // Navigate down to just above deepest level
        if ($_i < count($_path) - 1) {
          // Indent labels
          $indent = str_repeat('&nbsp;&nbsp;', $_i + 1);
          $option =& $option[$indent . $_path_segment];
        }
      }
      // Set item
      $option["$prefix$_path_segment"] = $_item;
    }
  }
}
