======================
= ANU content styles =
======================

By Taihao Zhang (zhang.taihao@gmail.com)

Version 2 style updates by Chris Skene (chris@xtfer.com)

Drupal 7 port by Asad Sultan (asad.sultan@anu.edu.au)

------------------------------------------------------------------------------

This module enables configuration of ANU content styles to panes in the Panels
module and regular blocks.

The configuration form configures the following settings:

  settings[box][show_title] : string

      Show the title in a separate header box if value is "header".

  settings[box][style] : string

      The base box CSS class to use.

  settings[content][bg] : string

      The background style CSS class for the base box.

  settings[content][fg] : string

      Text colour CSS class used to override the base box.

  settings[border][style] : string

      The style CSS class of the border to override the base box.

  settings[border][color] : string

      The border colour CSS class over the base box.

For Panels panes and regions, the configuration and rendering are handled in
the module itself. Configuration is done via "Styles" settings in panel layout
content settings. Rendering is then handled at page processing time.

For blocks, the module provides a variable called 'anu_content_styles' for the template.
As of this version, the parameters contained are:

  anu_content_styles[show_box_header] : boolean

      Show the title in a separate box header.

  anu_content_styles[styles] : string

      An array of CSS classes the configuration translates into.
