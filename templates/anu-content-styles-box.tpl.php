<?php
/**
 * @file
 * ANU box template.
 *
 * Implements ANU styles for boxes.
 */

if (!empty($anu_content_styles)) {
  extract($anu_content_styles);
}

?>
<div class="anu-box <?php echo $classes; ?>">
  <?php if ($show_box_header && !empty($title)) { ?>
  <div class="box-header"><?php print render($title); ?></div>
  <?php } ?>
  <div class="<?php echo $styles_classes; ?>">
    <div class="anu-box-content">
      <?php echo $content; ?>
    </div>
  </div>
</div>
