<?php
/**
 * @file
 * Table style extender.
 */

/**
 * Provides CSS class option for table style.
 */
class anu_content_styles_views_table_extender extends views_plugin_display_extender {
  function options_definition_alter(&$options) {
    $options['anu_table_class'] = array(
      'default' => NULL,
    );
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    if ($form_state['section'] == 'style_options' && $this->display->get_plugin('style') instanceof views_plugin_style_table) {
      // Retrieve option.
      $anu_table_class = $this->display->get_option('anu_table_class');
      // Retrieve value from legacy field.
      $style_options = $this->display->get_option('style_options');
      $style_options += array('anu_table_class' => '');
      // Add option form.
      $form['style_options']['anu_table_class'] = array(
        '#type' => 'textfield',
        '#title' => t('ANU table classes'),
        '#default_value' => $anu_table_class ? $anu_table_class : $style_options['anu_table_class'],
        '#description' => t('Enter the CSS classes from the !styleguide for styling a table with customized appearance.', array('!styleguide' => l(t('ANU Web Style Guide - Tables'), 'http://styles.anu.edu.au/guide/tables.php', array('attributes' => array('target' => '_blank'))))),
        '#weight' => -2,
      );
    }
  }

  function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);

    if ($form_state['section'] == 'style_options') {
      $anu_table_class = $form_state['values']['style_options']['anu_table_class'];
      // Move value from style options into display options.
      unset($form_state['values']['style_options']['anu_table_class']);
      $form_state['values']['anu_table_class'] = $anu_table_class ? $anu_table_class : NULL;
    }
  }

  function options_submit(&$form, &$form_state) {
    parent::options_submit($form, $form_state);

    if ($form_state['section'] == 'style_options') {
      $this->display->set_option('anu_table_class', $form_state['values']['anu_table_class']);
    }
  }
}
