<?php
/**
 * @file
 * ANU pane styles.
 *
 * Implements ANU styles for panes.
 */

$plugin = array(
  'title' => t('ANU styles'),
  'description' => t('Style the pane with options from the ANU Web Style Guide.'),
  'render region' => 'anu_content_styles_anu_pane_style_render_region',
  'render pane' => 'anu_content_styles_anu_pane_style_render_pane',
  'settings form' => 'anu_content_styles_anu_pane_settings_form',
  'settings form validate' => 'anu_content_styles_anu_pane_settings_form_validate',
  'pane settings form' => 'anu_content_styles_anu_pane_pane_settings_form',
  'pane settings form validate' => 'anu_content_styles_anu_pane_pane_settings_form_validate',
);

function anu_content_styles_anu_pane_settings_form($settings) {
  $settings += array('content' => array());
  $settings['content'] += array('region_style' => '');

  module_load_include('inc', 'anu_content_styles', 'includes/config_form');
  $form = anu_content_styles_titleless_config_form($settings);
  $form['content']['region_style'] = array(
    '#type' => 'select',
    '#title' => t('Pane style'),
    '#options' => array(
      '' => t('<Default>'),
      'ul' => t('Unordered list'),
      'ol' => t('Ordered list'),
    ),
    '#default_value' => $settings['content']['region_style'],
    '#weight' => -5,
  );
  return $form;
}

function anu_content_styles_anu_pane_settings_form_validate(&$form, &$form_state) {
  module_load_include('inc', 'anu_content_styles', 'includes/config_form');
  return anu_content_styles_config_form_validate($form, $form_state);
}

function anu_content_styles_anu_pane_pane_settings_form($settings) {
  module_load_include('inc', 'anu_content_styles', 'includes/config_form');
  return anu_content_styles_config_form($settings);
}

function anu_content_styles_anu_pane_pane_settings_form_validate(&$form, &$form_state) {
  module_load_include('inc', 'anu_content_styles', 'includes/config_form');
  return anu_content_styles_config_form_validate($form, $form_state);
}

/**
 * Render callback.
 * @themeable
 */
function theme_anu_content_styles_anu_pane_style_render_region($variables) {
  $panes = $variables['panes'];
  $settings = $variables['settings'];

  if (!empty($settings['content']['region_style']) && in_array($settings['content']['region_style'], array('ul', 'ol'))) {
    $items = array();

    $pane_content = '';
    if (!empty($panes)) {
      foreach ($panes as $pane_id => $item) {
        $items[] = '<li>' . $item . '</li>';
      }
      $pane_content .= '<' . $settings['content']['region_style'] . '>';
      $pane_content .= implode($items);
      $pane_content .= '</' . $settings['content']['region_style'] . '>';
    }
  }
  else {
    $pane_content = theme('panels_default_style_render_region__anu_content_style', $variables);
  }

  // Render the box.
  $settings = $variables['settings'];
  return theme('anu_content_styles_box', array(
    'title' => '',
    'content' => $pane_content,
    'settings' => $settings,
  ));
}

/**
 * Render callback.
 * @themeable
 */
function theme_anu_content_styles_anu_pane_style_render_pane($variables) {
  $content = &$variables['content'];
  $settings = $variables['settings'];

  $extra = array();
  $title = $content->title;
  // Render the default pane to output first
  if (isset($settings['box']['show_title']) && $settings['box']['show_title'] == 'header') {
    // Clear title before rendering as pane
    $content->title = '';
  }

  // Scratch custom CSS classes and reserve for box only
  if (!empty($content->css_class)) {
    $extra['css_class'] = $content->css_class;
    $content->css_class = '';
  }

  $pane_content = theme('panels_pane', $variables);

  // Render the box.
  return theme('anu_content_styles_box', array(
    'title' => $title,
    'content' => $pane_content,
    'settings' => $settings,
    'extra' => $extra,
  ));
}
